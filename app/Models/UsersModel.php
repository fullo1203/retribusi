<?php

namespace App\Models;

use CodeIgniter\Model;

use function PHPUnit\Framework\returnSelf;

class UsersModel extends Model
{
    protected $table = 'users';
    protected $primaryKey = 'users_id';
    protected $allowedFields = ['users_nama', 'users_nip', 'users_email', 'users_password', 'users_alamat', 'users_telp', 'created_at', 'role_id'];


    public function getUsers($users_id = false)
    {
        if ($users_id == false) {
            return $this->findAll();
        }

        return $this->where(['users_id' => $users_id])->first();
    }
}
