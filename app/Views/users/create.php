<?= $this->extend('layout/template'); ?>

<?= $this->section('content'); ?>
<div class="container">
    <div class="row">
        <div class="col-8">
            <h2>Form Tambah Users</h2>
            <?php if (session()->getFlashdata('pesan')) : ?>
                <div class="alert alert-success" role="alert">
                    <?= session()->getFlashdata('pesan'); ?>
                </div>
            <?php endif; ?>
            <form action="/users/save" method="post">
                <?= csrf_field(); ?>
                <div class="row mb-3">
                    <label for="Nama" class="col-sm-2 col-form-label">NAMA</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control <?= ($validation->hasError('users_nama')) ? 'is-invalid' : ''; ?>" id="users_nama" name="users_nama" autofocus value="<?= old('users_nama'); ?>">
                        <div class=" invalid-feedback">
                            <?= $validation->getError('users_nama'); ?>
                        </div>
                    </div>
                </div>
                <div class="row mb-3">
                    <label for="nip" class="col-sm-2 col-form-label">NIP</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control <?= ($validation->hasError('users_nip')) ? 'is-invalid' : ''; ?>" id="users_nip" name="users_nip" autofocus value="<?= old('users_nip'); ?>">
                        <div class=" invalid-feedback">
                            <?= $validation->getError('users_nip'); ?>
                        </div>
                    </div>
                </div>
                <div class="row mb-3">
                    <label for="Email" class="col-sm-2 col-form-label">EMAIL</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control <?= ($validation->hasError('users_email')) ? 'is-invalid' : ''; ?>" id="users_email" name="users_email" autofocus value="<?= old('users_email'); ?>">
                        <div class=" invalid-feedback">
                            <?= $validation->getError('users_email'); ?>
                        </div>
                    </div>
                </div>
                <div class="row mb-3">
                    <label for="Email" class="col-sm-2 col-form-label">PASSWORD</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control <?= ($validation->hasError('users_email')) ? 'is-invalid' : ''; ?>" id="users_email" name="users_email" autofocus value="<?= old('users_email'); ?>">
                        <div class=" invalid-feedback">
                            <?= $validation->getError('users_email'); ?>
                        </div>
                    </div>
                </div>
                <div class="row mb-3">
                    <label for="alamat" class="col-sm-2 col-form-label">ALAMAT</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control <?= ($validation->hasError('users_alamat')) ? 'is-invalid' : ''; ?>" id="users_alamat" name="users_alamat" autofocus value="<?= old('users_alamat'); ?>">
                        <div class=" invalid-feedback">
                            <?= $validation->getError('users_alamat'); ?>
                        </div>
                    </div>
                </div>
                <div class="row mb-3">
                    <label for="telepon" class="col-sm-2 col-form-label">TELEPON</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control <?= ($validation->hasError('users_telp')) ? 'is-invalid' : ''; ?>" id="users_telp" name="users_telp" autofocus value="<?= old('users_telp'); ?>">
                        <div class=" invalid-feedback">
                            <?= $validation->getError('users_telp'); ?>
                        </div>
                    </div>
                </div>
                <div class="row mb-auto">
                    <label for="status" class="col-sm-2 col-form-label">STATUS</label>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="users_status" ?>
                        <label class="form-check-label" for="flexCheckDefault">
                            aktif
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="users_status" id="flexCheckChecked">
                        <label class="form-check-label" for="flexCheckChecked">
                            tidak aktif
                        </label>
                    </div>
                    <div class="row mb-3">
                        <label for="role" class="col-sm-2 col-form-label">ROLE</label>
                        <div class="col-sm-10">
                            <select class="form-select" name="role_id" aria-label="Default select example">
                                <option selected>Pilih Salah Satu Role</option>
                                <option value="1">Admin</option>
                                <option value="2">Operator</option>
                                <option value="3">Bendahara</option>
                                <option value="4">Opd</option>
                            </select>
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary">Tambah Data</button>
            </form>
        </div>
    </div>
</div>
<?= $this->endSection(); ?>