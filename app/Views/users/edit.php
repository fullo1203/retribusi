<?= $this->extend('layout/template'); ?>

<?= $this->section('content'); ?>
<div class="container">
    <div class="row">
        <div class="col-8">
            <h2>Form Ubah Data Petugas</h2>
            <form action="/users/update/<?= $users['users_id']; ?>" method="post">
                <?= csrf_field(); ?>
                <input type="hidden" name="users_id=" value="<?= $users['users_id']; ?>">
                <div class="row mb-3">
                    <label for="Nama" class="col-sm-2 col-form-label">NAMA</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control <?= ($validation->hasError('users_nama')) ? 'is-invalid' : ''; ?>" id="users_nama" name="users_nama" autofocus value=<?= $users['users_nama']; ?>>
                        <div class=" invalid-feedback">
                            <?= $validation->getError('users_nama'); ?>
                        </div>
                    </div>
                </div>
                <div class="row mb-3">
                    <label for="nip" class="col-sm-2 col-form-label">NIP</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control <?= ($validation->hasError('users_nip')) ? 'is-invalid' : ''; ?>" id="users_nip" name="users_nip" autofocus value=<?= $users['users_nip']; ?>>
                        <div class=" invalid-feedback">
                            <?= $validation->getError('users_nip'); ?>
                        </div>
                    </div>
                </div>
                <div class="row mb-3">
                    <label for="Email" class="col-sm-2 col-form-label">EMAIL</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control <?= ($validation->hasError('users_email')) ? 'is-invalid' : ''; ?>" id="users_email" name="users_email" autofocus value=<?= $users['users_email']; ?>>
                        <div class=" invalid-feedback">
                            <?= $validation->getError('users_email'); ?>
                        </div>
                    </div>
                </div>
                <div class="row mb-3">
                    <label for="Password" class="col-sm-2 col-form-label">PASSWORD</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control <?= ($validation->hasError('users_password')) ? 'is-invalid' : ''; ?>" id="users_password" name="users_password" autofocus value=<?= $users['users_password']; ?>>
                        <div class=" invalid-feedback">
                            <?= $validation->getError('users_password'); ?>
                        </div>
                    </div>
                </div>
                <div class="row mb-3">
                    <label for="alamat" class="col-sm-2 col-form-label">ALAMAT</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control <?= ($validation->hasError('users_alamat')) ? 'is-invalid' : ''; ?>" id="users_alamat" name="users_alamat" autofocus value=<?= $users['users_alamat']; ?>>
                        <div class=" invalid-feedback">
                            <?= $validation->getError('users_alamat'); ?>
                        </div>
                    </div>
                </div>
                <div class="row mb-3">
                    <label for="telepon" class="col-sm-2 col-form-label">TELEPON</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control <?= ($validation->hasError('users_telp')) ? 'is-invalid' : ''; ?>" id="users_telp" name="users_telp" autofocus value=<?= $users['users_telp']; ?>>
                        <div class=" invalid-feedback">
                            <?= $validation->getError('users_telp'); ?>
                        </div>
                    </div>
                </div>
                <div class="row mb-auto">
                    <label for="status" class="col-sm-2 col-form-label">STATUS</label>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" <?= ($validation->hasError('users_status')) ? 'is-invalid' : ''; ?>" id="users_status" name="users_status" autofocus value=<?= $users['users_status']; ?>>
                        <label class="form-check-label" for="flexCheckDefault">
                            aktif
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="" id="flexCheckChecked">
                        <label class="form-check-label" for="flexCheckChecked">
                            tidak aktif
                        </label>
                    </div>
                </div>
                <div class="row mb-3">
                    <label for="role" class="col-sm-2 col-form-label">ROLE</label>
                    <div class="col-sm-10">
                        <select class="form-select  <?= ($validation->hasError('role_id')) ? 'is-invalid' : ''; ?>" id="role_id" name="role_id" autofocus value=<?= $users['role_id']; ?>>
                            <option selected>Pilih sesuai Akses</option>
                            <option value="1">Admin</option>
                            <option value="2">Operator</option>
                            <option value="3">Bendahara</option>
                            <option value="4">Opd</option>
                        </select>
                        <?= $validation->getError('role_id'); ?>
                    </div>
                </div>
        </div>
        <button type="submit" class="btn btn-primary">ubah Data</button>
        </form>
    </div>
</div>
</div>
<?= $this->endSection(); ?>