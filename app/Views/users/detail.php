<?= $this->extend('layout/template'); ?>

<?= $this->section('content'); ?>
<div class="container">
    <div class="row">
        <div class="col">
            <h2>Detail Users</h2>
            <div class="card mb-3" style="max-width: 540px;">
                <div class="row g-0">
                    <div class="col-md-8">
                        <div class="card-body">
                            <p class="card-text"><b>NAMA : </b><?= $users['users_nama']; ?></p>
                            <p class="card-text"><b>NIP : </b><?= $users['users_nip']; ?></p>
                            <p class="card-text"><b>EMAIL : </b><?= $users['users_email']; ?></p>
                            <p class="card-text"><b>PASSWORD : </b><?= $users['users_password']; ?></p>
                            <p class="card-text"><b>ALAMAt : </b><?= $users['users_alamat']; ?></p>
                            <p class="card-text"><b>TELEPON : </b><?= $users['users_telp']; ?></p>
                            <p class="card-text"><b>STATUS : </b><?= $users['users_status']; ?></p>
                            <p class="card-text"><b>ROLE : </b><?= $users['role_id']; ?></p>
                            <a href="/users/edit/<?= $users['users_id']; ?>" class="btn btn-warning">Edit</a>

                            <form action="/users/delete/<?= $users['users_id']; ?>" method="post" class="d-inline">
                                <?= csrf_field(); ?>
                                <input type="hidden" name="_method" value="DELETE">
                                <button type="submit" class="btn btn-danger" onclick="return confirm('apakah anda yakin?');">Delete</button>
                            </form>
                            <br><br>
                            <a href="/users">Kembali ke daftar users</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection(); ?>